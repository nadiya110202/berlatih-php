<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Document</title>
    </head>

    <body>
        <h1>Mengubah Huruf</h1>
        <?php
            function geser_huruf($huruf)
            {
                $abjad = "abcdefghijklmnopqrstuvwxyz";
                $idx = stripos($abjad, $huruf);
                if ($idx !== false) {
                    return $abjad[$idx + 1];
                }
                return $huruf;
            }

            function ubah_huruf($string){
                $geser = "";
                for ($i=0; $i < strlen($string); $i++) { 
                    $geser .= geser_huruf($string[$i]);
                }
                return $geser;
            }

            // TEST CASES
            echo ubah_huruf('wow'); // xpx
            echo "<br>";
            echo ubah_huruf('developer'); // efwfmpqfs
            echo "<br>";
            echo ubah_huruf('laravel'); // mbsbwfm
            echo "<br>";
            echo ubah_huruf('keren'); // lfsfo
            echo "<br>";
            echo ubah_huruf('semangat'); // tfnbohbu

            ?>

    </body>
</html>
